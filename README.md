# Super Sparty Brothers

## About

Super Sparty Brothers is a 2D platform game that I developed while taking a course
titled [Game Development for Modern Platforms](https://www.coursera.org/learn/gamedev-platforms) on Coursera.
I created the game using [Unity](http://www.unity3d.com).

## Features

* Extra life hearts
* Invincible (super) shields
* Enemies called archers that shoot arrows

## Screenshots

![New level](http://www.bjpeterdelacruz.com/games/SuperSpartyBros/images/Screenshot_856x642_3.png)

![Super shield](http://www.bjpeterdelacruz.com/games/SuperSpartyBros/images/Screenshot_856x642_4.png)

![Super Sparty and knocked-out enemy](http://www.bjpeterdelacruz.com/games/SuperSpartyBros/images/Screenshot_856x642_5.png)

## Links

[My Website](http://www.bjpeter.com)

[My Blog](http://www.samuraijournal.com)