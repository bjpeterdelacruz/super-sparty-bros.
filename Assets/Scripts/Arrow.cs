﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour {

	public int damage;
	public GameObject explosion;
	public AudioClip destroySFX;

	void OnTriggerEnter2D (Collider2D other)
	{
		if ((other.tag == "Player" ) && (other.gameObject.GetComponent<CharacterController2D>().playerCanMove))
		{
			CharacterController2D controller = other.gameObject.GetComponent<CharacterController2D>();

			if (explosion) {
				Instantiate(explosion, transform.position, transform.rotation);
			}
			if (controller.playerIsInvincible) {
				controller.PlaySound(destroySFX);
			} else {
				controller.ApplyDamage(damage);
			}
			DestroyObject(this.gameObject);
		}
	}
}
