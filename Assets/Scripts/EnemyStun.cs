﻿using UnityEngine;
using System.Collections;

public class EnemyStun : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Player")
		{
			CharacterController2D controller = other.gameObject.GetComponent<CharacterController2D>();

			if (this.GetComponentInParent<Enemy>() != null) {
				this.GetComponentInParent<Enemy>().Stunned();
			} else if (this.GetComponentInParent<Archer>() != null) {
				this.GetComponentInParent<Archer>().Stunned();
			}

			controller.EnemyBounce();
		}
	}

}
